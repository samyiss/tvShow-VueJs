import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import DetailsView from '@/views/DetailsView.vue';
import SeasonEpisodesView from '@/views/SeasonEpisodesView.vue';
import DetailsEpisodeView from '@/views/DetailsEpisodeView.vue';
import LoginView from '@/views/LoginView.vue';
import AboutView from '@/views/AboutView.vue';
import SignupView from '@/views/SignupView.vue';
import ProfileView from '@/views/ProfileView.vue';
import JouerEpisodeView from '@/views/JouerEpisodeView.vue';
import HistoryView from '@/views/HistoryView.vue';
import FavoritesView from '@/views/FavoritesView.vue';

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView,
    },
    {
        path: '/details/:tvshowId',
        name: 'details',
        component: DetailsView,
    },
    {
        path: '/history',
        name: 'history',
        component: HistoryView,
    },
    {
        path: '/episodes/:seasonId',
        name: 'season',
        component: SeasonEpisodesView,
    },
    {
        path: '/episode/:episodeId',
        name: 'episodedetails',
        component: DetailsEpisodeView,
    },
    {
        path: '/ViewEpisode/:episodeId',
        name: 'ViewEpisode',
        component: JouerEpisodeView,
    },
    {
        path: '/login',
        name: 'login',
        component: LoginView,
    },
    {
        path: '/favorites',
        name: 'favorites',
        component: FavoritesView,
    },
    {
        path: '/about',
        name: 'about',
        component: AboutView,
    },
    {
        path: '/signup',
        name: 'signup',
        component: SignupView,
    },
    {
        path: '/profile',
        name: 'profile',
        component: ProfileView,
    },
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
});

export default router;
