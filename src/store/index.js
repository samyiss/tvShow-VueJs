import { createStore } from 'vuex';

export default createStore({
    state: {
        token: '',
        episodes: [],
        episodesId: [],
    },
    getters: {},
    mutations: {
        setToken(state, token) {
            state.token = token;
        },
        setEpisode(state, Epi) {
            state.episodes = Epi;
            if (Epi.length !== 0) {
                state.episodesId = [];
                Epi.forEach((element) => {
                    state.episodesId.push(element.episodeId);
                });
            }
        },
    },
    actions: {
        storeToken(context, token) {
            context.commit('setToken', token);
            if (token !== '') sessionStorage.setItem('token', token);
            else sessionStorage.removeItem('token');
        },
        storeEpisode(context, Epi) {
            context.commit('setEpisode', Epi);
        },
    },
    modules: {},
});
