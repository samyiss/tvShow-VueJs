const svrURL = 'https://tvshowdbapi.herokuapp.com';

const regexUser = /^[a-zA-Z0-9_]{5,20}$/;

const regexPassWd = /^.{6,}$/;

const regexEmail = /^.{4,}$/;

const logout = (t) => {
    t.$store.dispatch('storeToken', '');
    t.$store.dispatch('storeEpisode', []);
    t.$router.push({ path: '/' });
};

const verification = (email, username, password, Confpassword) => {
    let message = '';
    if (username === '' || password === '' || email === '' || Confpassword === '') {
        if (!regexEmail.test(email) || !email.includes('@')) message += 'le courriel doit contenir le symbole @ \nle courriel doit contenir plus de 4 caractères \n';
        if (!regexUser.test(username)) message += 'Le username doit contenir au moins 5 caractères et au maximum 20. \nles caractères permis sont A-Z, a-z, 0-9 et le caractère souligné \n';
        if (!regexPassWd.test(password)) message += 'le mot de passe doit contenir au moins 6 caractères \n';
        if (Confpassword === '' && password !== '') message += 'Veuillez saisir votre mot de passe de confirmation\n';
        return message;
    }
    if (Confpassword !== password && Confpassword !== '' && password !== '') {
        message = 'la confirmation et le mot de passe doit être identique au mot de passe\n';
        return message;
    }
    return message;
};

const getHistory = async (t) => {
    const rep = await fetch(`${svrURL}/user/history`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            authorization: t.$store.state.token,
        },
    });
    if (rep.ok) {
        t.$store.dispatch('storeEpisode', await rep.json());
    }
};

// eslint-disable-next-line import/prefer-default-export
export {
    svrURL, logout, getHistory, verification, regexUser, regexPassWd, regexEmail,
};
